﻿namespace SofaWarriors
{

    public enum AttackType : short
    {
        Melee=0,   //eg Sword, Bat
        Bullet=1,  //eg Bow, Fireball
        Instant=2, //eg Curse
        Mass=3     //eg Poison rain
    }

}