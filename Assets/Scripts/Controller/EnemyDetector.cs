﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SofaWarriors.Controller
{

    public class EnemyDetector : MonoBehaviour
    {

        public event System.Action<string> OnUnitDetected;

        int[] enemyTeams;

        [SerializeField]
        SphereCollider sphereColider;

        public void Init(int[] _enemyTeams, float _radius)
        {
            enemyTeams = _enemyTeams;
            sphereColider.radius = _radius;
        }



    }
}