﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SofaWarriors.Model;

namespace SofaWarriors.Controller
{
    public abstract class AttackComponent : MonoBehaviour
    {

        public System.Action OnAttackReady;
        public System.Action<AttackInfo> OnAttack;

        protected float attackReloadTime { get; set; }

        public virtual void Attack(AttackInfo attackInfo)
        {
            OnAttack(attackInfo);
            ReloadAttak();            
        }       

        public virtual void ReloadAttak()
        {
            StartCoroutine(WaitForNextAttack(attackReloadTime));
        }

        IEnumerator WaitForNextAttack( float delay )
        {
            yield return new WaitForSeconds(delay);
            if (OnAttackReady != null)
                OnAttackReady();
        }


    }
}