﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SofaWarriors.Model;

public class EnemyFindComponent : MonoBehaviour
{

    [SerializeField]
    EnemyFindParameters _parameters;
	
	public EnemyFindParameters parameters
    {
        get { return _parameters; }
    }
}
