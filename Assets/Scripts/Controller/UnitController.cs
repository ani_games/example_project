﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SofaWarriors.Model;

namespace SofaWarriors.Controller
{

    public class UnitController : MonoBehaviour
    {
        [SerializeField]
        EnemyFindComponent enemyFinder;
        [SerializeField]
        AttackComponent attackController;
        [SerializeField]
        UnityEngine.AI.NavMeshAgent agent;

        [SerializeField]
        EnemyDetector enemyDetector;

        [SerializeField]
        int team, enemyTeam;

        void Start()
        {
            enemyDetector.Init(new int[] { enemyTeam }, enemyFinder.parameters.detectionRange);
        }

        void Update()
        {

        }
    }
}