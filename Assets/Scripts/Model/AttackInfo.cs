﻿using SofaWarriors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SofaWarriors.Model;

namespace SofaWarriors.Model
{
    public struct AttackInfo
    {
        /// <summary>
        /// Type of attack
        /// </summary>
        public AttackType type { get; private set; }
        /// <summary>
        /// Amount of damage
        /// </summary>
        public float damage { get; private set; }
        /// <summary>
        /// Start point of Attack
        /// </summary>
        public Vector3 origin { get; private set; }
        /// <summary>
        /// Final point of Attack
        /// </summary>
        public Vector3 destination { get; private set; }
        /// <summary>
        /// Speed of attack, eg speed of bullet or fireball
        /// </summary>
        public float spread { get; private set; }

        public AttackInfo(AttackType _type, float _damage, Vector3 _origin, Vector3 _destination, float _spread)
        {
            type = _type;
            damage = _damage;
            origin = _origin;
            destination = _destination;
            spread = _spread;
        }

        public static AttackInfo MeleeAtack(float _damage, Vector3 _origin, Vector3 _destination)
        {
            return new AttackInfo(AttackType.Melee, _damage, _origin, _destination, 1f);
        }

    }
}