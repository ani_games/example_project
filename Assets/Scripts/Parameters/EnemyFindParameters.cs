﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SofaWarriors.Model
{

    public class EnemyFindParameters : ScriptableObject
    {
        [SerializeField]
        float _detectionRange;
        [SerializeField]
        float _attackRange;

        public float detectionRange
        {
            get { return _detectionRange; }
        }

        public float attackRange
        {
            get { return _attackRange; }
        }

    }

}