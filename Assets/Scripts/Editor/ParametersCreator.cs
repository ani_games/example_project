﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using SofaWarriors.Model;
using UnityEngine;

namespace SofaWarriors
{

    public class ParametersCreator
    {

        [MenuItem("Assets/Create/Parameters/EnemyFindParameters")]
        public static void CreateEnemyFindParameters()
        {
            EnemyFindParameters asset = ScriptableObject.CreateInstance<EnemyFindParameters>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/EnemyFind.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }

    }
}